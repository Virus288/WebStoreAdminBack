export interface errorInterface {
  reason: string;
  message: string;
}
