import { NextFunction } from 'express';
import { errorInterface } from './interfaces/errInterface';

export class errorsHandler {
  public handle(error: errorInterface, next?: NextFunction): void {
    console.log('handieling err');
    if (next) {
      next(error);
    }
  }
}
