import { createLogger, format } from 'winston';
import DailyRotateFile = require('winston-daily-rotate-file');

export const logger = createLogger({
  transports: [
    new DailyRotateFile({
      level: 'info',
      filename: './logs/errors-%DATE%.log',
      json: true,
      format: format.combine(
        format.timestamp(),
        format.align(),
        format.printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`),
      ),
      datePattern: 'yyyy-MM-DD',
      maxFiles: 20,
      handleExceptions: true,
      handleRejections: true,
    }),
    new DailyRotateFile({
      level: 'error',
      filename: './logs/errors-%DATE%.log',
      json: true,
      format: format.combine(
        format.timestamp(),
        format.align(),
        format.printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`),
      ),
      datePattern: 'yyyy-MM-DD',
      maxFiles: 20,
      handleExceptions: true,
      handleRejections: true,
    }),
  ],
});
