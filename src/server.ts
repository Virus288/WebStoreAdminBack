import express from 'express';
import http from 'http';
import { rootRouter } from './routers/rootRouter';
import { genericMiddleware } from './middleware/genericMiddleware';
import { mongoDB } from './tools/mongodb';
import { errorHandler } from './errorsHandling/errorsMiddleware';

const app = express();

app.use(genericMiddleware);
app.use(rootRouter);
app.use(errorHandler);

try {
  mongoDB().then(() => {
    http.createServer(app).listen(5024, () => {
      console.log('Connected. Listening on http 5024');
    });
  });
} catch (error) {
  console.log(error);
}
