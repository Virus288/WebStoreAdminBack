import nodemailer from 'nodemailer';

import getConfig from '../../../tools/configLoader';
import { emailData } from '../../generic/interfaces/genericInterface';

export const mailer = async (config: emailData): Promise<void> => {
  const transporter = nodemailer.createTransport({
    host: getConfig('host').host,
    port: getConfig('port').port,
    secure: getConfig('secure').secure,
    auth: {
      user: getConfig('emailUser').emailUser,
      pass: getConfig('emailPassword').emailPassword,
    },
  });
  await transporter.sendMail({
    from: `"${config.senderName}" ${config.sender}`,
    to: config.receiver,
    subject: config.subject,
    text: config.message,
    html: config.html,
  });
};
