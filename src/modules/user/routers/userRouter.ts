import { Router as router } from 'express';
import { UsersController } from '../controllers/usersController';
import * as globalState from '../../../globalStore/storeController/storeController';
import { removeTokens, sendJwtToken } from '../utils/utils';
import { isLogged } from '../../generic/security/securityValidator';
import { AlreadyLoggedIn, JwtError, JwtErrors } from '../../generic/errors/errors';

export const userRouter = router();
export const securedUserRouter = router();

userRouter.post('/login', (req, res, next) => {
  if (isLogged()) throw new AlreadyLoggedIn();
  const user = new UsersController();

  user
    .loginUser(req.body)
    .then((id: string) => sendJwtToken(res, id))
    .catch((err) => next(err));
});

securedUserRouter.get('/login', (_req, res) => {
  res.send(true);
});

securedUserRouter.delete('/account/remove', (req, res) => {
  const user = new UsersController();

  user.remove(req.body).then((data) => {
    removeTokens(res);
    res.send(data);
  });
});

securedUserRouter.patch('/account/update', (req, res, next) => {
  const user = new UsersController();
  const userId = globalState.getUserId();
  if (!userId) throw new JwtError(JwtErrors.tokenNotValid);

  user
    .updateData(req.body, userId)
    .then((data) => res.send(data))
    .catch((err) => next(err));
});

securedUserRouter.get('/account/validate/:validationUrl', (req, res, next) => {
  const user = new UsersController();
  const userId = globalState.getUserId();
  if (!userId) throw new JwtError(JwtErrors.tokenNotValid);

  user
    .validateAccount(userId, req.params.validationUrl)
    .then((data) => res.send(data))
    .catch((err) => next(err));
});
