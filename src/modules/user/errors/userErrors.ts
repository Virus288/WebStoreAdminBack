export enum emailErrors {
  emailNotProvidedError = 'Email does not exist',
  emailIncorrectError = 'Email is not correct',
  emailAlreadyExistsError = 'Email is already registered',
  emailNotRegisteredError = 'Email is not registered',
}

export enum loginErrors {
  passwordIncorrectError = 'Password is incorrect',
  passwordNotProvidedError = 'Password or repeated password does not exist',
  passwordNotCorrectError = 'Password is not correct',
  passwordsNotSameError = 'Passwords are not the same',
}

export class EmailError extends Error {
  constructor(message: emailErrors) {
    super('EmailError');
    this.message = message;
    this.name = 'EmailError';
  }
}

export class LoginError extends Error {
  constructor(message: loginErrors) {
    super('LoginError');
    this.message = message;
    this.name = 'LoginError';
  }
}

export class BadValidationUrl extends Error {
  constructor() {
    super('BadValidationUrl');
    this.message = 'Validation url is wrong';
    this.name = 'BadValidationUrl';
  }
}

export class AlreadyValidated extends Error {
  constructor() {
    super('AlreadyValidated');
    this.message = 'Account already validated';
    this.name = 'AlreadyValidated';
  }
}

export class NoUserData extends Error {
  constructor() {
    super('NoUserData');
    this.message = 'User data was not provided';
    this.name = 'NoUserData';
  }
}
