import {
  IUser,
  LoginSuccessInterface,
  UserLoginInterface,
  UserQueryInterface,
  UsersInterface,
} from '../interfaces/usersInterface';
import { User } from '../schemas/userSchema';
import { RegisterSuccess, RemovingAccountSuccess, TokenValidated, UpdatingDataSuccess } from '../utils/callbacks';
import { createRandomString, createSalt } from '../utils/utils';
import {
  checkIfTokenAlreadyValidated,
  userDataValidation,
  validateLoginPassword,
  validateToken,
} from '../validation/usersValidator';
import { EmailError, emailErrors } from '../errors/userErrors';

export class UsersController {
  async create(userProperties: UsersInterface): Promise<LoginSuccessInterface> {
    userDataValidation(userProperties);
    userProperties.password = await createSalt(userProperties.password);
    const data = createRandomString(75);

    const newUser: IUser = new User({ ...userProperties, validationToken: data });
    userProperties.password = await createSalt(userProperties.password);
    return this.saveUser(newUser);
  }

  async loginUser(user: UserLoginInterface): Promise<string> {
    const userData: UsersInterface = await this.findOneByType({ email: user.email });
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);
    await validateLoginPassword(user.password, userData.password);

    return userData._id;
  }

  async remove(userProperties: UserLoginInterface): Promise<LoginSuccessInterface> {
    const userData: UsersInterface = await this.findOneByType({ email: userProperties.email });
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);

    return this.delete(userData._id);
  }

  async updateData(userProperties: UserQueryInterface, userId: string): Promise<LoginSuccessInterface> {
    const userData: UsersInterface = await this.findOneById(userId);
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);

    return this.update(userData._id as string, userProperties);
  }

  async validateAccount(userId: string, validationUrl: string): Promise<LoginSuccessInterface> {
    const userData: UsersInterface = await this.findOneById(userId);
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);
    checkIfTokenAlreadyValidated(userData);

    return this.validate(userId, validationUrl, userData.validationToken as string);
  }

  private async findOneById(id: string): Promise<UsersInterface> {
    return await User.findOne({ _id: id });
  }

  private async findOneByType(query: UserQueryInterface): Promise<UsersInterface> {
    return await User.findOne({ ...query });
  }

  private async saveUser(newUser: IUser): Promise<LoginSuccessInterface> {
    const userData = await this.findOneByType({ email: newUser.email });

    if (userData != undefined) throw new EmailError(emailErrors.emailAlreadyExistsError);
    return await newUser.save().then(() => {
      return RegisterSuccess;
    });
  }

  private async delete(id: string): Promise<LoginSuccessInterface> {
    await User.findOneAndRemove({ _id: id });
    return RemovingAccountSuccess;
  }

  private async update(id: string, userNewData: UserQueryInterface): Promise<LoginSuccessInterface> {
    // #note updating user email should have different type of validation same as changing password
    const userData = await User.findOneAndUpdate({ _id: id }, { ...userNewData }, { new: true });
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);
    return UpdatingDataSuccess;
  }

  private async validate(userId: string, validationUrl: string, userToken: string): Promise<LoginSuccessInterface> {
    validateToken(validationUrl, userToken);
    await this.update(userId, { verified: true });
    return TokenValidated;
  }
}
