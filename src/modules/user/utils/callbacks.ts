export const RegisterSuccess = {
  success: true,
  message: 'Register successful',
};

export const RemovingAccountSuccess = {
  success: true,
  message: 'Account removed',
};

export const UpdatingDataSuccess = {
  success: true,
  message: 'Account updated',
};

export const TokenValidated = {
  success: true,
  message: 'Account validated. Enjoy full access to all features',
};
