import bcrypt from 'bcrypt';
import { Response } from 'express';
import jwt from 'jsonwebtoken';
import * as enums from '../enums/enums';
import getConfig from '../../../tools/configLoader';
import { mailer } from '../../mailer/controller/mailController';

export const createSalt = async (password: string): Promise<string> => {
  const salt = await bcrypt.genSalt();
  return await bcrypt.hash(password, salt);
};

export const createRandomString = (length: number): string => {
  const data = 'abcdefghijklmnouprstuwzxv1234567890(_[{]}<>';
  let string = '';
  for (let x = 0; x < length; x++) {
    string += data.at(Math.random() * 43);
  }
  return string;
};

export const sendJwtToken = (res: Response, id: string): void => {
  const JWT = jwt.sign({ id: id }, getConfig('jwt').jwt, { expiresIn: enums.JwtData.tokenMaxAge });
  const JWTRefresh = jwt.sign({ id: id }, getConfig('jwtRefresh').jwtRefresh, {
    expiresIn: enums.JwtData.refreshTokenMaxAge,
  });
  res.cookie('JWT', JWT, { httpOnly: true, maxAge: enums.JwtData.tokenMaxAge * 1000 });
  res.cookie('JWTRefresh', JWTRefresh, { httpOnly: true, maxAge: enums.JwtData.refreshTokenMaxAge * 1000 });
  res.status(200).json({ success: true, message: 'Logged in successfully' });
};

export const refreshToken = (res: Response, id: string): void => {
  const JWTRefresh = jwt.sign({ id: id }, getConfig('jwtRefresh').jwtRefresh, {
    expiresIn: enums.JwtData.refreshTokenMaxAge,
  });

  res.cookie('JWTRefresh', JWTRefresh, { httpOnly: true, maxAge: enums.JwtData.refreshTokenMaxAge * 1000 });
};

export const removeTokens = (res: Response): void => {
  res.cookie('JWT', 'removingToken', { httpOnly: true, maxAge: 0 });
  res.cookie('JWTRefresh', 'removingToken', { httpOnly: true, maxAge: 0 });
};

export const sendRegistrationEmail = (receiver: string): void => {
  mailer({
    message: 'validation code and message',
    sender: getConfig('emailUser').emailUser,
    senderName: getConfig('emailUsername').emailUsername,
    receiver: receiver,
    subject: 'test',
  })
    .then()
    .catch((err) => console.log(err));
};
