import * as mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: [true, 'Please provide valid email'],
      unique: true,
    },
    password: {
      type: String,
      required: [true, 'Please provide valid password'],
      minlength: [6, 'Min password lenght is 6 characters'],
      maxlength: [100, 'Max password lenght is 100 characters'],
    },
    verified: {
      type: Boolean,
      required: false,
      default: 'false',
    },
    validationToken: {
      type: String,
      required: [true, 'Please provide validaion string'],
      minlength: [70],
    },
  },
  { timestamps: false },
);

export const User = mongoose.model('user', userSchema);
