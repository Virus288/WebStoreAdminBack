import mongoose from 'mongoose';

export interface IUser extends mongoose.Document {
  _id: string;
  email: string;
  password: string;
  verified: boolean;
}

export interface UsersInterface {
  _id: string;
  email: string;
  password: string;
  password2?: string;
  verified: boolean;
  validationToken?: string;
}

// User interface for find query
export interface UserQueryInterface {
  _id?: string;
  email?: string;
  password?: string;
  verified?: boolean;
}

export interface UserLoginInterface {
  email: string;
  password: string;
}

export interface LoginSuccessInterface {
  success: boolean;
  message: string;
}
