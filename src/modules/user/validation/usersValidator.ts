import bcrypt from 'bcrypt';
import PasswordValidator from 'password-validator';
import validator from 'validator';
import { UsersInterface } from '../interfaces/usersInterface';
import * as errors from '../errors/userErrors';

export const userDataValidation = (userProperties: UsersInterface): void => {
  genericValidator(userProperties);
  validateEmail(userProperties.email);
  validatePassword(userProperties.password, userProperties.password2);
};

const genericValidator = (userProperties: UsersInterface): void => {
  if (!userProperties) throw new errors.NoUserData();
};

export const validateEmail = (email: string): void => {
  if (email == undefined) throw new errors.EmailError(errors.emailErrors.emailNotProvidedError);
  if (!validator.isEmail(email)) throw new errors.EmailError(errors.emailErrors.emailIncorrectError);
};

// Validate password
const validatePassword = (password: string, password2: string | undefined): void => {
  if (password === undefined || password2 === undefined) {
    throw new errors.LoginError(errors.loginErrors.passwordNotProvidedError);
  }
  if (password !== password2) throw new errors.LoginError(errors.loginErrors.passwordsNotSameError);

  const schema = new PasswordValidator();
  schema.is().min(6).is().max(100).has().uppercase().has().lowercase().has().digits(1).has().not().spaces();
  if (!schema.validate(password)) throw new errors.LoginError(errors.loginErrors.passwordNotCorrectError);
};

export const validateLoginPassword = async (password: string, hashedPassword: string): Promise<void> => {
  const auth = bcrypt.compare(password, hashedPassword);
  if (!auth) throw new errors.LoginError(errors.loginErrors.passwordIncorrectError);
};

export const validateToken = (validateUrl: string, userToken: string): void => {
  if (validateUrl !== userToken) throw new errors.BadValidationUrl();
};

export const checkIfTokenAlreadyValidated = (userData: UsersInterface): void => {
  if (userData.verified) throw new errors.AlreadyValidated();
};
