import jwt from 'jsonwebtoken';

export interface ConfigInterface {
  mongoUrl: string;
  cors: string;
  jwt: string;
  jwtRefresh: string;
  host: string;
  port: number;
  secure: boolean;
  emailUser: string;
  emailUsername: string;
  emailPassword: string;
}

export interface JWTData extends jwt.JwtPayload {
  id: string;
}

export interface emailData {
  message: string;
  sender: string;
  senderName: string;
  receiver: string;
  subject: string;
  html?: string;
}
