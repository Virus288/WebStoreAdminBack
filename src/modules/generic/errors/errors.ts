export enum JwtErrors {
  tokenNotValid = 'User not logged in',
  refreshTokenInvalid = 'Refresh token is invalid',
}

export class ConfigLineDoesNotExist extends Error {
  constructor(type: string) {
    super('ConfigLineDoesNotExist');
    this.message = `${type} line does not exist`;
    this.name = 'ConfigLineDoesNotExist';
  }
}

export class JwtError extends Error {
  constructor(message: JwtErrors) {
    super('JwtError');
    this.message = message;
    this.name = 'JwtError';
  }
}

export class NoConfigFile extends Error {
  constructor() {
    super('NoConfigFile');
    this.message = 'Config file does not exist';
    this.name = 'NoConfigFile';
  }
}

export class AlreadyLoggedIn extends Error {
  constructor() {
    super('AlreadyLoggedIn');
    this.message = 'User already logged in';
    this.name = 'AlreadyLoggedIn';
  }
}
