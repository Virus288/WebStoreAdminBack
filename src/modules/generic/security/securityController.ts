import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import getConfig from '../../../tools/configLoader';
import { JwtError, JwtErrors } from '../errors/errors';
import { JWTData } from '../interfaces/genericInterface';
import * as globalState from '../../../globalStore/storeController/storeController';
import { refreshToken } from '../../user/utils/utils';

export const validateIfLoggedIn = (jwtAccessToken: string, jwtRefreshToken: string, res: Response): void => {
  if (jwtRefreshToken) {
    const payload = jwt.verify(jwtRefreshToken, getConfig('jwtRefresh').jwtRefresh) as JWTData;
    return globalState.addUserId(payload.id);
  } else if (jwtAccessToken) {
    const payload = jwt.verify(jwtAccessToken, getConfig('jwt').jwt) as JWTData;
    if (payload) refreshToken(res, payload.id);
    return globalState.addUserId(payload.id);
  }
};

export const validateRoutes = (_req: Request, _res: Response, next: NextFunction): void => {
  if (globalState.getUserId() == undefined) throw new JwtError(JwtErrors.tokenNotValid);

  next();
};
