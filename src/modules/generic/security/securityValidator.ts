import * as globalStore from '../../../globalStore/storeController/storeController';

export const isLogged = (): boolean => {
  return globalStore.getUserId() != undefined;
};
