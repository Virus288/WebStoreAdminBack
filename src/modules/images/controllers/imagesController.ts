import { ImageType } from '../interfaces/imagesInterface';
import { ProductsController } from '../../products/controllers/productsController';
import { ImageInterface, ImageQueryInterface } from '../../products/interfaces/productsInterface';
import { LoginSuccessInterface } from '../../user/interfaces/usersInterface';

export class ImagesController {
  addImageProduct = async (newProduct: {
    productId: string;
    productType: ImageType['type'];
    fileName: string;
  }): Promise<LoginSuccessInterface> => {
    const product = new ProductsController();
    const images: ImageQueryInterface = {};

    switch (newProduct.productType) {
      case 'additionalImages':
        images.images = newProduct.fileName;
        break;
      case 'mainImage':
        images.mainImage = newProduct.fileName;
        break;
      case 'icon':
        images.icon = newProduct.fileName;
        break;
    }

    return await product.updateData({ image: images as ImageInterface, _id: newProduct.productId });
  };
}
