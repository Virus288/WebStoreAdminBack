import multer from 'multer';
import { ImageEnums } from '../enums/enums';
import { FileFilterCallback, Request } from '../interfaces/imagesInterface';
import { ImageError, ImageErrors } from '../errors/errors';

// Multer data
const storage = multer.diskStorage({
  destination: (_req, _file, callBack) => {
    callBack(null, ImageEnums.LOCATION);
  },
  filename: (_req, file, callBack) => {
    callBack(null, file.originalname);
  },
});

const fileFilter = (_req: Request, file: Express.Multer.File, callback: FileFilterCallback): void => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
    callback(null, true);
  } else {
    callback(new ImageError(ImageErrors.wrongImageType));
  }
};

const fileLimits = {
  fileSize: 1024 * 1024 * 5, // 5mb
};

// Save images
export const saveImage = multer({ storage, limits: fileLimits, fileFilter });
