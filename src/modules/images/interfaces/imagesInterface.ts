import { Readable } from 'stream';

export interface FileFilterCallback {
  (error: Error): void;

  (error: null, acceptFile: boolean): void;
}

export interface Request {
  file?: File | undefined;
  files?:
    | {
        [fieldname: string]: File[];
      }
    | File[]
    | undefined;
}

export interface File {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  size: number;
  stream: Readable;
  destination: string;
  filename: string;
  path: string;
  buffer: Buffer;
}

export interface ImageType {
  type: 'icon' | 'mainImage' | 'additionalImages';
}
