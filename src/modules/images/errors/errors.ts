export enum ImageErrors {
  imageAlreadyExists = 'Image with that name already exists',
  wrongImageType = 'File has wrong data type. Allowed data types are png, jpg and jpeg',
  imageDoesNotExist = 'File not provided',
}

export class ImageError extends Error {
  constructor(message: ImageErrors) {
    super('ImageError');
    this.message = message;
    this.name = 'ImageError';
  }
}
