import { Router as router } from 'express';
import { UserDataController } from '../controllers/userDataController';
import * as globalState from '../../../globalStore/storeController/storeController';
import { UserDataInterface } from '../interfaces/userDataInterface';
import { LoginSuccessInterface } from '../../user/interfaces/usersInterface';

export const userDataRouter = router();

userDataRouter.get('/user/data', (_req, res, next) => {
  const userData = new UserDataController();
  const userId = globalState.getUserId();

  userData
    .getData(userId as string)
    .then((data: UserDataInterface) => res.send(data))
    .catch((err) => next(err));
});

userDataRouter.put('/user/data', (req, res, next) => {
  const userData = new UserDataController();
  const userId = globalState.getUserId();
  const body = { ...req.body, userId: userId };

  userData
    .create(body)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});

userDataRouter.patch('/user/data', (req, res, next) => {
  const userData = new UserDataController();
  const userId = globalState.getUserId();
  const body = { ...req.body, userId: userId };

  userData
    .updateData(body)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});

userDataRouter.delete('/user/data', (_req, res, next) => {
  const userData = new UserDataController();
  const userId = globalState.getUserId();

  userData
    .remove(userId as string)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});
