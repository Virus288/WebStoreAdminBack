import * as mongoose from 'mongoose';

const userDataSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, 'Please provide valid email'],
    unique: true,
  },
  userId: {
    type: String,
    required: [true, 'Please provide userId'],
    unique: true,
  },
});

export const UserData = mongoose.model('userData', userDataSchema);
