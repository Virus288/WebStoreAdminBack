export const AddingDataSuccessful = {
  success: true,
  message: 'Data added',
};

export const UpdatingDataSuccess = {
  success: true,
  message: 'Data updated',
};

export const RemovingSuccessful = {
  success: true,
  message: 'Data removed successful',
};
