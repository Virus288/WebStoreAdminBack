export class UserDataAlreadyExists extends Error {
  constructor() {
    super('UserDataAlreadyExists');
    this.message = 'User data already exists';
    this.name = 'UserDataAlreadyExists';
  }
}

export class NoUserData extends Error {
  constructor() {
    super('NoUserData');
    this.message = 'User data does not exists';
    this.name = 'NoUserData';
  }
}
