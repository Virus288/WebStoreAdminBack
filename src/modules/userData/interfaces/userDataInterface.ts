import * as mongoose from 'mongoose';

// Mongoose interface
export interface IUserData extends mongoose.Document {
  userId: string;
  email: string;
}

export interface UserDataInterface {
  userId: string;
  email: string;
}

export interface UserDataQueryInterface {
  userId?: string;
  email?: string;
}
