import { IUserData, UserDataInterface, UserDataQueryInterface } from '../interfaces/userDataInterface';
import { UserData } from '../schemas/userDataSchema';
import { AddingDataSuccessful, RemovingSuccessful } from '../utils/utils';
import { validateEmail } from '../../user/validation/usersValidator';
import { NoUserData, UserDataAlreadyExists } from '../errors/userDataErrors';
import { LoginSuccessInterface } from '../../user/interfaces/usersInterface';
import { UpdatingDataSuccess } from '../../products/utils/utils';

export class UserDataController {
  async create(userData: UserDataInterface): Promise<LoginSuccessInterface> {
    const user = await this.findOneByType({ userId: userData.userId });
    if (user) throw new UserDataAlreadyExists();
    validateEmail(userData.email);

    const newData: IUserData = new UserData({ email: userData.email, userId: userData.userId });
    return this.saveData(userData.userId, newData);
  }

  async remove(userId: string): Promise<LoginSuccessInterface> {
    const user = await this.findOneByType({ userId });
    if (!user) throw new NoUserData();

    return this.delete(userId);
  }

  async updateData(userData: UserDataInterface): Promise<LoginSuccessInterface> {
    const user: UserDataInterface = await this.findOneByType({ userId: userData.userId });
    if (!user) throw new NoUserData();

    return this.update(userData);
  }

  async getData(userId: string): Promise<UserDataInterface> {
    const user: UserDataInterface = await this.findOneByType({ userId });
    if (!user) throw new NoUserData();

    return user;
  }

  private async findOneByType(query: UserDataQueryInterface): Promise<UserDataInterface> {
    const userData = await UserData.findOne({ ...query });
    if (!userData) throw new NoUserData();
    return userData;
  }

  private async saveData(userId: string, newData: IUserData): Promise<LoginSuccessInterface> {
    const userData = await this.findOneByType({ userId });

    if (!userData) throw new UserDataAlreadyExists();
    return await newData.save().then(() => {
      return AddingDataSuccessful;
    });
  }

  private async delete(userId: string): Promise<LoginSuccessInterface> {
    const userData = await UserData.findOneAndRemove({ userId });
    if (!userData) throw new NoUserData();
    return RemovingSuccessful;
  }

  private async update(userData: UserDataInterface): Promise<LoginSuccessInterface> {
    const user = await UserData.findOneAndUpdate({ userId: userData.userId }, { ...userData }, { new: true });
    if (!user) throw new NoUserData();
    return UpdatingDataSuccess;
  }
}
