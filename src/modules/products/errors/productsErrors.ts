export enum ProductDataErrors {
  tooShort = 'is too short.',
  tooLong = 'is too short.',
}

export class NoProduct extends Error {
  constructor() {
    super('NoProduct');
    this.message = 'Product does not exists';
    this.name = 'NoProduct';
  }
}

export class NoProductData extends Error {
  constructor(dataType: string) {
    super('NoProductData');
    this.message = `Product is missing ${dataType}`;
    this.name = 'NoProductData';
  }
}

export class DataInvalid extends Error {
  constructor(dataType: string, reason: ProductDataErrors, explanation: string) {
    super('DataInvalid');
    this.message = `Property ${dataType} ${reason} ${explanation}`;
    this.name = 'DataInvalid';
  }
}
