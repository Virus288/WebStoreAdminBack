import { IProduct, ProductsInterface, ProductsQueryInterface } from '../interfaces/productsInterface';
import { Product } from '../schemas/productsSchema';
import { AddingDataSuccessful, RemovingSuccessful, UpdatingDataSuccess } from '../utils/utils';
import { validateProduct } from '../validation/productValidation';
import { NoProduct } from '../errors/productsErrors';
import { LoginSuccessInterface } from '../../user/interfaces/usersInterface';

export class ProductsController {
  async create(product: ProductsInterface): Promise<LoginSuccessInterface> {
    validateProduct(product);

    const newProduct: IProduct = new Product({ ...product });
    return this.saveData(newProduct);
  }

  async remove(productId: string): Promise<LoginSuccessInterface> {
    const product = await this.findOneById(productId);
    if (!product) throw new NoProduct();

    return this.delete(productId);
  }

  async updateData(productData: ProductsQueryInterface): Promise<LoginSuccessInterface> {
    const product: ProductsInterface = await this.findOneById(productData._id);
    if (!product) throw new NoProduct();

    return this.update(productData);
  }

  async getData(): Promise<ProductsInterface[]> {
    const product: ProductsInterface[] = await this.findAll();
    if (!product) throw new NoProduct();

    return product;
  }

  async getByType(type: ProductsQueryInterface['type'], id?: string): Promise<ProductsInterface[] | ProductsInterface> {
    if (id) {
      const product: ProductsInterface[] = await this.findAllByType(type);
      const filtered = product.filter((product) => product._id === id);
      if (!filtered) throw new NoProduct();
      return filtered;
    } else {
      const product: ProductsInterface[] = await this.findAllByType(type);
      if (!product) throw new NoProduct();
      return product;
    }
  }

  async getOne(id: string): Promise<ProductsInterface> {
    const product: ProductsInterface = await this.findOneById(id);
    if (!product) throw new NoProduct();

    return product;
  }

  private async findAll(): Promise<ProductsInterface[]> {
    const product: ProductsInterface[] = await Product.find();
    if (!product) throw new NoProduct();
    return product;
  }

  private async findAllByType(type: ProductsQueryInterface['type']): Promise<ProductsInterface[]> {
    const product = await Product.findOne({ type: type });
    return product;
  }

  private async findOneById(productId: string): Promise<ProductsInterface> {
    const product = await Product.findOne({ _id: productId });
    return product;
  }

  private async saveData(newProduct: IProduct): Promise<LoginSuccessInterface> {
    return await newProduct.save().then(() => {
      return AddingDataSuccessful;
    });
  }

  private async delete(productId: string): Promise<LoginSuccessInterface> {
    const userData = await Product.findOneAndRemove({ _id: productId });
    if (!userData) throw new NoProduct();
    return RemovingSuccessful;
  }

  private async update(productData: ProductsQueryInterface): Promise<LoginSuccessInterface> {
    const user = await Product.findOneAndUpdate({ _id: productData._id }, { ...productData }, { new: true });
    if (!user) throw new NoProduct();
    return UpdatingDataSuccess;
  }
}
