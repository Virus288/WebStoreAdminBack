import { ProductsInterface } from '../interfaces/productsInterface';
import { DataInvalid, NoProductData, ProductDataErrors } from '../errors/productsErrors';

export const validateProduct = (product: ProductsInterface): void => {
  const { name, price, type, amount, size, color, fullDescription, shortDescription } = product;

  if (!name) throw new NoProductData('name');
  if (!price) throw new NoProductData('price');
  if (!type) throw new NoProductData('type');
  if (!amount) throw new NoProductData('amount');
  if (!size) throw new NoProductData('size');
  if (!color) throw new NoProductData('color');

  if (!shortDescription) throw new NoProductData('shortDescription');
  if (shortDescription.length < 10)
    throw new DataInvalid('shortDescription', ProductDataErrors.tooShort, 'Min lenght is 10 characters');
  if (shortDescription.length < 10)
    throw new DataInvalid('shortDescription', ProductDataErrors.tooLong, 'Max lenght is 100 characters');

  if (!fullDescription) throw new NoProductData('fullDescription');
  if (fullDescription.length < 10)
    throw new DataInvalid('fullDescription', ProductDataErrors.tooShort, 'Min lenght is 10 characters');
  if (fullDescription.length < 10)
    throw new DataInvalid('fullDescription', ProductDataErrors.tooLong, 'Max lenght is 300 characters');
};
