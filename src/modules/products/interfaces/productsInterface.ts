import * as mongoose from 'mongoose';

// Mongoose interface
export interface IProduct extends mongoose.Document {
  _id: string;
  name: string;
  price: number;
  type: 'posciele' | 'przescieradla' | 'poduszki' | 'koce' | 'koldry' | 'reczniki';
  amount: number;
  size: string;
  color: string;
  fullDescription: string;
  shortDescription: string;
  image?: ImageInterface;
}

export interface ProductsInterface {
  _id: string;
  name: string;
  price: number;
  type: 'posciele' | 'przescieradla' | 'poduszki' | 'koce' | 'koldry' | 'reczniki';
  amount: number;
  size: string;
  color: string;
  fullDescription: string;
  shortDescription: string;
  image?: ImageInterface;
}

export interface ProductsQueryInterface {
  _id: string;
  name?: string;
  price?: number;
  type?: 'posciele' | 'przescieradla' | 'poduszki' | 'koce' | 'koldry' | 'reczniki';
  amount?: number;
  size?: string;
  color?: string;
  fullDescription?: string;
  shortDescription?: string;
  image?: ImageInterface;
}

export interface ImageInterface {
  mainImage: string;
  icon: string;
  images: Array<string>;
}

export interface ImageQueryInterface {
  mainImage?: string;
  icon?: string;
  images?: string;
}
