import * as mongoose from 'mongoose';

const imageSchema = new mongoose.Schema({
  icon: {
    type: String,
    required: false,
  },
  mainImage: {
    type: String,
    required: false,
  },
  additionalImages: {
    type: String,
    required: false,
  },
});

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter product name'],
    },
    price: {
      type: Number,
      required: [true, 'Please enter product price'],
    },
    type: {
      type: String,
      required: [true, 'Please enter type of product'],
    },
    amount: {
      type: Number,
      required: [true, 'Please enter available amount'],
    },
    size: {
      type: String,
      required: [true, 'Please enter available size'],
    },
    color: {
      type: String,
      required: [true, 'Please enter available colors'],
    },
    fullDescription: {
      type: String,
      required: [true, 'Please enter full description'],
      minlength: [10, 'Min lenght of full description is 10 characters'],
      maxlength: [300, 'Max lenght of full description is 300 characters'],
    },
    shortDescription: {
      type: String,
      required: [true, 'Please enter short description'],
      minlength: [10, 'Min lenght of short description is 10 characters'],
      maxlength: [100, 'Max lenght of short description is 100 characters'],
    },
    image: {
      type: imageSchema,
      required: false,
    },
  },
  { timestamps: false },
);

export const Product = mongoose.model('product', productSchema);
