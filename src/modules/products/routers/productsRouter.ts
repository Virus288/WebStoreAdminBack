import { Router as router } from 'express';
import { ProductsController } from '../controllers/productsController';
import { LoginSuccessInterface } from '../../user/interfaces/usersInterface';
import { saveImage } from '../../../tools/images/imageHandler';
import { ImageError, ImageErrors } from '../../../tools/images/errors/errors';
import { ProductsInterface, ProductsQueryInterface } from '../interfaces/productsInterface';
import { ImagesController } from '../../images/controllers/imagesController';

export const productsRouter = router();
export const securedProductsRouter = router();

productsRouter.get('/products', (_req, res, next) => {
  const products = new ProductsController();

  products
    .getData()
    .then((data: ProductsInterface[]) => res.send(data))
    .catch((err) => next(err));
});

productsRouter.get('/products/:category', (req, res, next) => {
  const products = new ProductsController();

  products
    .getByType(req.params.category as ProductsQueryInterface['type'])
    .then((data: ProductsInterface) => res.send(data))
    .catch((err) => next(err));
});

productsRouter.get('/products/:category/:id', (req, res, next) => {
  const products = new ProductsController();

  products
    .getByType(req.params.category as ProductsQueryInterface['type'], req.params.id)
    .then((data: ProductsInterface) => res.send(data))
    .catch((err) => next(err));
});

securedProductsRouter.put('/products', (req, res, next) => {
  const products = new ProductsController();

  products
    .create(req.body)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});

securedProductsRouter.post('/images', saveImage.single('productImage'), (req, res, next) => {
  if (req.file === undefined) {
    next(new ImageError(ImageErrors.imageDoesNotExist));
  }
  return res.send({
    success: true,
    message: 'Image uploaded successfully',
  });
});

securedProductsRouter.patch('/images', (req, res, next) => {
  const controller = new ImagesController();

  console.log(req.body);

  controller
    .addImageProduct(req.body)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});

securedProductsRouter.delete('/products/:category/:id', (req, res, next) => {
  const products = new ProductsController();

  products
    .remove(req.body)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});

securedProductsRouter.patch('/products/:category/:id', (req, res, next) => {
  const products = new ProductsController();

  products
    .updateData(req.body)
    .then((data: LoginSuccessInterface) => res.send(data))
    .catch((err) => next(err));
});
