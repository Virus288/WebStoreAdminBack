import { Router as router } from 'express';
import { securedUserRouter, userRouter } from '../modules/user/routers/userRouter';
import { userDataRouter } from '../modules/userData/routers/userDataRouter';
import { productsRouter, securedProductsRouter } from '../modules/products/routers/productsRouter';
import { validateRoutes } from '../modules/generic/security/securityController';

export const rootRouter = router();

rootRouter.use(userRouter);
rootRouter.use(productsRouter);

rootRouter.use(validateRoutes);
rootRouter.use(securedUserRouter);
rootRouter.use(securedProductsRouter);
rootRouter.use(userDataRouter);

// 404
rootRouter.get('/*', (req, res) => {
  res.status(404).json({ message: `Cannot ${req.method} ${req.url}`, error: 'Not Found' });
});

rootRouter.post('/*', (req, res) => {
  res.status(404).json({ message: `Cannot ${req.method} ${req.url}`, error: 'Not Found' });
});
