import getConfig from './configLoader';
import mongoose, { ConnectOptions } from 'mongoose';

export const mongoDB = async (): Promise<void> => {
  await mongoose.connect(getConfig('mongoUrl').mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectOptions);
};
