import devConfig from '../../config/devConfig.json';
import prodConfig from '../../config/prodConfig.json';
import { ConfigLineDoesNotExist, NoConfigFile } from '../modules/generic/errors/errors';
import { ConfigInterface } from '../modules/generic/interfaces/genericInterface';

/**
 * Get json config file
 * @returns {json} Returns json file for functions to use.
 */
export default function getConfig(line: string): ConfigInterface {
  switch (process.env.NODE_ENV) {
    case 'dev':
      if (!devConfig[line as keyof ConfigInterface]) throw new ConfigLineDoesNotExist(line);
      return devConfig as ConfigInterface;
    case 'prod':
      if (!prodConfig[line as keyof ConfigInterface]) throw new ConfigLineDoesNotExist(line);
      return prodConfig;
    default:
      throw new NoConfigFile();
  }
}
