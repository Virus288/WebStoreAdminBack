import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import * as globalStore from '../globalStore/storeController/storeController';
import { validateIfLoggedIn } from '../modules/generic/security/securityController';
import getConfig from '../tools/configLoader';

export const genericMiddleware = express();

genericMiddleware.use(express.json());
genericMiddleware.use(cookieParser());
genericMiddleware.use(
  cors({
    origin: getConfig('cors').cors,
    credentials: true,
  }),
);

genericMiddleware.use((req, res, next) => {
  res.header('Content-Type', 'application/json;charset=UTF-8');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  const { JWT, JWTRefresh } = req.cookies;
  if (JWT || JWTRefresh) validateIfLoggedIn(JWT, JWTRefresh, res);

  next();
});

genericMiddleware.use((_req, res, next) => {
  res.on('finish', () => {
    globalStore.cleanup();
  });

  next();
});
