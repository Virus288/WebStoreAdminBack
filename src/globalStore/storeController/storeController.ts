import { StoreInterface } from '../interfaces/storeInterface';

const globalStore: StoreInterface = {
  userId: undefined,
};

export const addUserId = (id: string): void => {
  globalStore.userId = id;
};

export const getUserId = (): string | undefined => {
  return globalStore.userId;
};

export const cleanup = (): void => {
  globalStore.userId = undefined;
};
