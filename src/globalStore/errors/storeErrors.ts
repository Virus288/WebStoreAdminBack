export class UserIdError extends Error {
  constructor() {
    super('UserIdError');
    this.message = 'User id already exists';
    this.name = 'UserIdError';
  }
}
